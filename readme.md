## React

A JAVASCRIPT LIBRARY FOR BUILDING USER INTERFACES

## Base

Firebase gives you the tools to develop high-quality apps, grow your user base, and earn more money. We cover the essentials so you can monetize your business and focus on your users. [Firebase website](https://firebase.google.com/docs/)	

## Install Project

 - clone project (git clone git@gitlab.com:Francie-Dev/RecipeBox.git)
 - npm install in project (You need node.js [click here](https://nodejs.org/en/))
 - npm start 
 - url: http://localhost:3000/home/Francis

## Official Documentation

Documentation for the library can be found on the [React website](https://facebook.github.io/react/docs/hello-world.html).

## Contributing

Thank you for considering contributing to the React ! The contribution guide can be found in the [React documentation](https://facebook.github.io/react/contributing/how-to-contribute.html).

### License

Copyright (c) 2013-present, Facebook, Inc [React license](https://github.com/facebook/react/blob/master/LICENSE)
