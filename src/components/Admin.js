import React from 'react';
import AjouterRecette from './AjouterRecette';
import base from '../base';

class Admin extends React.Component{

	state = {
		uid: null,
		owner: null
	};

	componentDidMount(){
		base.onAuth(user => {
			if (user) {
				this.traiterChangement(null, {user})
			}
		})
	};

	traiterChangement = (event, key) => {
		const recette = this.props.recettes[key];
		const majRecette = {
			...recette,
			[event.target.name]: event.target.value
		};
		this.props.majRecette(key, majRecette);
	};

	renderLogin = () => {
		return(
			<div className="login">
				<h2>Connecte toi pour créer tes recettes !</h2>
				<button className="facebook-button" onClick={() => this.connexion('facebook')}>Me connecter avec Facebook</button>
				<button className="twitter-button" onClick={() => this.connexion('twitter')}>Me connecter avec twitter</button>
			</div>
			)
	};

	renderAdmin = key => {
		const recette = this.props.recettes[key];
		return (
				<div className="card" key={key}>
				<form className="admin-form" >

					<input type="text" name="nom" placeholder="Nom de la recette" onChange={(e) => this.traiterChangement(e, key)} value={recette.nom} />

					<input type="text"  name="image" placeholder="Adresse de l'image" onChange={(e) => this.traiterChangement(e, key)} value={recette.image} />

					<textarea rows="3" name="ingredients" placeholder="Liste des ingrédients séparés par une virgule" onChange={(e) => this.traiterChangement(e, key)} value={recette.ingredients} ></textarea>

					<textarea rows="15" name="instructions" placeholder="Liste des instructions (une par ligne)" onChange={(e) => this.traiterChangement(e, key)} value={recette.instructions} ></textarea>

				</form>
				<button onClick={() => this.props.supprimerRecette(key)}>
				supprimer
				</button>
			</div>
			)
	};

	connexion = provider => {
		base.authWithOAuthPopup(provider, this.traiterConnexion);
	};

	deconnexion = () => {
		base.unauth();
		this.setState({ uid: null });
	};

	traiterConnexion = (err, authData) => {
		if (err) {
			console.log(err);
			return;
		}
		// recupere le nom de boite 
		const boxRef = base.database().ref(this.props.pseudo)
		// demander firebase les données
		boxRef.once('value', snapshot => {
			const data = snapshot.val() || {};

			// attribuer la box si elle n'est a personne 
			if (!data.owner) {
				boxRef.set({
					owner: authData.user.uid
				})
			}
			this.setState({
				uid: authData.user.uid,
				owner: data.owner || authData.user.uid
			})
		});
	};

	render(){

		const deconnexion = <button onClick={this.deconnexion}>Deconnexion</button>

		// si il existe un proprietaire
		if(!this.state.uid){
			return <div>{this.renderLogin()}</div>
		}

		// est ce que c'est le proprietire 
		if(this.state.uid !== this.state.owner){
			return(
				<div className="login">
				{this.renderLogin()}
					<p> ⚠ Tu n'es pas le proprietaire de cette boite à recettes.</p>
				</div>
			)
		}

		const adminCards = Object
			.keys(this.props.recettes)
			.map(this.renderAdmin);

		return(
			<div className="cards">
				<AjouterRecette ajouterRecette={this.props.ajouterRecette} />
					{adminCards}
				<footer>
					<button onClick={this.props.chargerRecette}>
						remplir
					</button>
					{deconnexion}
				</footer>
			</div>
		)
	}
	static propTypes = {
		recettes: React.PropTypes.object.isRequired,
		chargerRecette: React.PropTypes.func.isRequired,
		ajouterRecette: React.PropTypes.func.isRequired,
		supprimerRecette: React.PropTypes.func.isRequired,
		majRecette: React.PropTypes.func.isRequired
	}
}
export default Admin;