import React from 'react';
import Header from './Header';
import Admin from './Admin';
import Card from './Card';
// charge recettes
import recettes from '../recettes';

// bdd
import base from '../base';

class App extends React.Component{
	
	state = {
		recettes: {}
	};

	componentWillMount(){
		this.ref = base.syncState(`${this.props.params.pseudo}/recettes`,
			{context: this,
			state: 'recettes'
		})
	}

	componentWillUnount(){
		base.removeBinding(this.ref);
	}

	chargerRecette = () => {
		this.setState({ recettes });
	};

	ajouterRecette = (recette) =>{
		const recettes = {...this.state.recettes};
		const timsestamp = Date.now();
		recettes[`recette-${timsestamp}`] = recette;
		this.setState({ recettes });
	};

	majRecette = (key, majRecette) => {
		const recettes = {...this.state.recettes};
		recettes[key] = majRecette;
		this.setState({ recettes});
	};

	supprimerRecette = key => {
		const recettes = {...this.state.recettes};
		recettes[key] = null;
		this.setState({ recettes });
	};

	render(){

		const getCard = Object
		.keys(this.state.recettes)
		.map(key => <Card  key={key} details={this.state.recettes[key]} />);

		return(
			<div className="box">
				<Header pseudo={this.props.params.pseudo} />
				<div className="cards">
					{getCard}
				</div>
				<Admin 
				recettes={this.state.recettes}
				chargerRecette={this.chargerRecette} 
				ajouterRecette={this.ajouterRecette}
				majRecette={this.majRecette}
				supprimerRecette={this.supprimerRecette}
				pseudo={this.props.params.pseudo}
				/>
			</div>
		)
	}

	static propTypes = {
		params: React.PropTypes.object.isRequired
	};
}

export default App; 