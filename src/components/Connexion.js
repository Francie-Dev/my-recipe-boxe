import React from 'react';


class Connexion extends React.Component{

	getPseudo = event => {
		event.preventDefault();
		const pseudo = this.pseudo.value;
		this.context.router.transitionTo(`/home/${pseudo}`);
	}
	render(){
		return(
			<div className="connexionBox">
				<form className="connexion" onSubmit={(e) => this.getPseudo(e)}>
					<h1> Ma boite à recette</h1>
					<input 
						required 
						type="text" 
						placeholder="Nom du chef" 
						pattern="[A-Za-z-]{1,}"
						ref={(input) => {this.pseudo = input}} 
					/>
					<button type="submit">Go</button>
					<p>Pas de caractères spéciaux.</p>
				</form>
			</div>
		)
	};

	static contextTypes = {
		router: React.PropTypes.objet
	};
}

export default Connexion; 